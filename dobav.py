from peewee import *

try:

 db = PostgresqlDatabase(host="localhost", port="6789", database="shop_dat1", user="postgres")

# Правильно было бы описать классы order, good, order_item в отдельном модуле и подключать этот модуль

 class order(Model):
  class Meta:
   database = db
   db_table = "orders"

 class good(Model):
  class Meta:
   datatabase = db
   db_table = "goods"

 class order_item(Model):
  order_item_id=AutoField(db_column="order_item_id", primary_key="True")
  forder_id=ForeignKeyField(order, db_column="order_id")
  fgood_id =ForeignKeyField(good,  db_column="good_id")
  quantity=IntegerField(db_column="quantity")
  class Meta:
   database = db
   db_table = "order_items"


 print("ДОБАВЛЕНИЕ товара в заказ")
 order1 = int(input("\n Введите id заказа: "))
 if order1 <= 0:
  raise Exception("Нет заказа с таким id")
 good1  = int(input("\n Введите id товара: "))
 if good1 <= 0:
  raise Exception("Нет товара с таким id")
 result = order_item.select().where((order_item.order_id==order1)&(order_item.good_id==good1));
 if result.count()>0:
  raise Exception("Такой товар уже есть в заказе")
 order_item1 = order_item.create(forder_id=order1, fgood_id=good1, quantity="1")

except (Exception) as e:
 print("Ошибка: " + str(e))
