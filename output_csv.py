from peewee import *
#from playhouse import * #.postgres_ext import *
from playhouse.dataset import DataSet
#from playhouse.csv_utils import *

try:
 db = PostgresqlDatabase(host="localhost", port="6789", database="shop_dat1", user="postgres")
 class customer(Model):
  cust_id=AutoField(db_column="cust_id", primary_key="True")
  first_name=CharField(db_column="first_name", max_length="100")
  last_name=CharField(db_column="last_name", max_length="100")
  class Meta:
   database = db
   db_table = "customers"

 #customer.create_table()
 #IvanIvanov = customer.create(first_name="Ivan",last_name="Ivanov")
 #PetrPetrov = customer.create(first_name="Petr",last_name="Petrov")
 IvanIvanov = customer.get(customer.cust_id=="1")
 PetrPetrov = customer.get(customer.cust_id=="2")

 #ds = DataSet('sqlite:///:memory:')
 #ds.freeze(customer.select(customer.first_name, customer.cust_id), format='csv', filename='dz07/orders.csv')

 #with open('1.csv',"w") as f:
 # query = customer.select()
 # dump_csv(query,f)


 class order(Model):
  order_id=AutoField(db_column="order_id", primary_key="True")
  fcust_id=ForeignKeyField(customer, db_column="cust_id")
  order_dttm=DateTimeField(db_column="order_dttm")
  status=CharField(db_column="status", max_length="20")
  class Meta:
   database = db
   db_table = "orders"

 #order.create_table()
 #order1 = order.create(fcust_id=IvanIvanov,order_dttm="2018-07-01 10:34:55",status="Ne gotov")
 #order2 = order.create(fcust_id=PetrPetrov,order_dttm="2018-07-02 11:34:55",status="Ne gotov")
 #order3 = order.create(fcust_id=IvanIvanov,order_dttm="2018-07-03 12:34:55",status="Ne gotov")
 order1 = order.get(order.order_id=="1")
 order2 = order.get(order.order_id=="2")
 order3 = order.get(order.order_id=="3")

 class good(Model):
  good_id=AutoField(db_column="good_id", primary_key="True")
  vendor=CharField(db_column="vendor", max_length="100")
  name=CharField(db_column="name", max_length="100")
  description=CharField(db_column="description", max_length="300")
  class Meta:
   database = db
   db_table = "goods"

 #good.create_table()
 #Cap = good.create(vendor='Adidas',name='Cap',description='Desc 1')
 #Boots = good.create(vendor='Nike',name='Boots',description='Desc 2')
 Cap  = good.get(good.good_id=="1")
 Boots= good.get(good.good_id=="2")

 class order_item(Model):
  order_item_id=AutoField(db_column="order_item_id", primary_key="True")
  forder_id=ForeignKeyField(order, db_column="order_id")
  fgood_id =ForeignKeyField(good,  db_column="good_id")
  quantity=IntegerField(db_column="quantity")
  class Meta:
   database = db
   db_table = "order_items"

 #order_item.create_table()
 #order_item1 = order_item.create(forder_id=order1,fgood_id=Cap,  quantity="1")
 #order_item2 = order_item.create(forder_id=order2,fgood_id=Boots,quantity="2")
 #order_item3 = order_item.create(forder_id=order3,fgood_id=Cap,  quantity="3")
 #order_item4 = order_item.create(forder_id=order3,fgood_id=Boots,quantity="4")

 ds = DataSet('sqlite:///:memory:')
 query = good.select(order.order_id,customer.first_name,good.name,good.vendor,order_item.quantity).join(order_item).join(order).join(customer)
 ds.freeze(query, format='csv', filename='dz07/orders.csv')

except (Exception) as error:
 print(error)
